object frmDashboard: TfrmDashboard
  Left = 0
  Top = 0
  Caption = 'Dashboard'
  ClientHeight = 520
  ClientWidth = 949
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 949
    Height = 257
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 1
    ExplicitTop = 6
    ExplicitWidth = 714
    ExplicitHeight = 225
    object cxGrid2ChartViewPie: TcxGridChartView
      DiagramColumn.Active = True
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGrid2ChartViewPie
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 257
    Width = 949
    Height = 263
    Align = alBottom
    TabOrder = 1
    ExplicitLeft = 1
    ExplicitTop = 237
    ExplicitWidth = 720
    object cxGridChartViewColumn: TcxGridChartView
      DiagramColumn.Active = True
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGridChartViewColumn
    end
  end
  object DataSource1: TDataSource
    DataSet = cdsFuncionario
    Left = 360
    Top = 200
  end
  object sqqFuncionario: TSQLQuery
    MaxBlobSize = -1
    Params = <>
    SQL.Strings = (
      'SELECT COUNT(*) AS TOTAL, C.DESCRICAO_FUNCAO'
      'FROM'
      'FUNCIONARIO F'
      'LEFT JOIN FUNCAO C ON (C.CODIGO_FUNCAO = F.CODIGO_FUNCAO)'
      'GROUP BY 2')
    SQLConnection = DMConexao.sqcConexaoPrincipal
    Left = 360
    Top = 16
    object sqqFuncionarioTOTAL: TIntegerField
      FieldName = 'TOTAL'
      Required = True
    end
    object sqqFuncionarioDESCRICAO_FUNCAO: TStringField
      FieldName = 'DESCRICAO_FUNCAO'
      Size = 255
    end
  end
  object cdsFuncionario: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFuncionario'
    Left = 360
    Top = 136
    object cdsFuncionarioTOTAL: TIntegerField
      FieldName = 'TOTAL'
      Required = True
    end
    object cdsFuncionarioDESCRICAO_FUNCAO: TStringField
      FieldName = 'DESCRICAO_FUNCAO'
      Size = 255
    end
  end
  object dspFuncionario: TDataSetProvider
    DataSet = sqqFuncionario
    Left = 360
    Top = 80
  end
end
