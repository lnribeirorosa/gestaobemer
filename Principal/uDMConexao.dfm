object DMConexao: TDMConexao
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 406
  Width = 1261
  object sqcConexaoPrincipal: TSQLConnection
    ConnectionName = 'Banco'
    DriverName = 'FIREBIRD'
    LoginPrompt = False
    Params.Strings = (
      'drivername=FIREBIRD'
      'librarynameosx=libsqlfb.dylib'
      'vendorlibwin64=fbclient.dll'
      'vendorlibosx=/Library/Frameworks/Firebird.framework/Firebird'
      'blobsize=-1'
      'commitretain=False'
      'Database=C:\LN\Gestao\Database\Gestao.fdb'
      'localecode=0000'
      'password=masterkey'
      'rolename=RoleName'
      'sqldialect=3'
      'isolationlevel=ReadCommitted'
      'user_name=sysdba'
      'waitonlocks=True'
      'trim char=False')
    Connected = True
    Left = 88
    Top = 56
  end
  object dspUsuario: TDataSetProvider
    DataSet = fsqUsuario
    Left = 368
    Top = 112
  end
  object cdsFuncao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncao'
    Left = 280
    Top = 192
    object cdsFuncaoCODIGO_FUNCAO: TIntegerField
      Tag = 7
      DisplayLabel = 'Codigo'
      FieldName = 'CODIGO_FUNCAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsFuncaoDESCRICAO_FUNCAO: TStringField
      Tag = 7
      DisplayLabel = 'Descricao'
      FieldName = 'DESCRICAO_FUNCAO'
      Size = 255
    end
  end
  object dspFuncao: TDataSetProvider
    DataSet = fsqFucao
    Left = 280
    Top = 112
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\LN\Gestao\Database\Gestao.fdb'
      'Server=127.0.0.1/3051'
      'CharacterSet=WIN1252'
      'DriverID=FB')
    Connected = True
    Left = 88
    Top = 120
  end
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 96
    Top = 192
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 96
    Top = 248
  end
  object fsqUsuario: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT U.USUA_CODIGO, U.USUA_NOME, U.USUA_LOGIN,'
      '             U. USUA_SENHA'
      'FROM USUARIO U'
      'WHERE USUA_CODIGO =:CODIGO')
    Left = 368
    Top = 56
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object cdsUsuario: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspUsuario'
    Left = 368
    Top = 192
    object cdsUsuarioUSUA_CODIGO: TIntegerField
      Tag = 7
      DisplayLabel = 'Codigo'
      FieldName = 'USUA_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsUsuarioUSUA_NOME: TStringField
      Tag = 7
      DisplayLabel = 'Nome'
      FieldName = 'USUA_NOME'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 150
    end
    object cdsUsuarioUSUA_LOGIN: TStringField
      DisplayLabel = 'Login'
      FieldName = 'USUA_LOGIN'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsUsuarioUSUA_SENHA: TStringField
      DisplayLabel = 'Senha'
      FieldName = 'USUA_SENHA'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
  end
  object fsqFucao: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT F.CODIGO_FUNCAO, F.DESCRICAO_FUNCAO'
      'FROM FUNCAO F'
      'WHERE F.CODIGO_FUNCAO =:CODIGO')
    Left = 280
    Top = 48
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dspFuncionario: TDataSetProvider
    DataSet = fsqFuncionario
    Left = 456
    Top = 112
  end
  object fsqFuncionario: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT CODIGO_FUNCIONARIO,'
      'LOGIN_FUNCIONARIO,'
      'NOME_FUNCIONARIO,'
      'SALARIO_FUNCIONARIO,'
      'DTAADM_FUNCIONARIO,'
      'DTADEM_FUNCIONARIO,'
      'OBS_FUNCIONARIO,'
      'CODIGO_FUNCAO,'
      'FOTO_FUNCIONARIO'
      ''
      'FROM FUNCIONARIO F'
      ''
      'WHERE F.CODIGO_FUNCIONARIO =:CODIGO')
    Left = 456
    Top = 56
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsFuncionario: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncionario'
    Left = 456
    Top = 192
    object cdsFuncionarioCODIGO_FUNCIONARIO: TIntegerField
      Tag = 7
      DisplayLabel = 'Codigo'
      FieldName = 'CODIGO_FUNCIONARIO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsFuncionarioNOME_FUNCIONARIO: TStringField
      Tag = 7
      DisplayLabel = 'Nome'
      FieldName = 'NOME_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 65
    end
    object cdsFuncionarioSALARIO_FUNCIONARIO: TFMTBCDField
      DisplayLabel = 'Sal'#225'rio'
      FieldName = 'SALARIO_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Precision = 18
      Size = 2
    end
    object cdsFuncionarioDTAADM_FUNCIONARIO: TDateField
      DisplayLabel = 'Data Admiss'#227'o'
      FieldName = 'DTAADM_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsFuncionarioDTADEM_FUNCIONARIO: TDateField
      DisplayLabel = 'Data Desligamento'
      FieldName = 'DTADEM_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
    end
    object cdsFuncionarioOBS_FUNCIONARIO: TMemoField
      FieldName = 'OBS_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsFuncionarioCODIGO_FUNCAO: TIntegerField
      DisplayLabel = 'Fun'#231#227'o'
      FieldName = 'CODIGO_FUNCAO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsFuncionarioFOTO_FUNCIONARIO: TBlobField
      DisplayLabel = 'Foto'
      FieldName = 'FOTO_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsFuncionarioLOGIN_FUNCIONARIO: TStringField
      DisplayLabel = 'Login'
      FieldName = 'LOGIN_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 65
    end
  end
  object fsqFuncoes: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT F.CODIGO_FUNCAO, F.DESCRICAO_FUNCAO'
      'FROM FUNCAO F')
    Left = 552
    Top = 48
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsFuncoes: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncoes'
    Left = 552
    Top = 184
    object cdsFuncoesCODIGO_FUNCAO: TIntegerField
      FieldName = 'CODIGO_FUNCAO'
      Required = True
    end
    object cdsFuncoesDESCRICAO_FUNCAO: TStringField
      FieldName = 'DESCRICAO_FUNCAO'
      Size = 255
    end
  end
  object dspFuncoes: TDataSetProvider
    DataSet = fsqFuncoes
    Left = 552
    Top = 104
  end
  object dspTipoParecer: TDataSetProvider
    DataSet = fsqTipoParecer
    Left = 633
    Top = 104
  end
  object fsqTipoParecer: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT P.CODIGO_TIPO_PARECER, P.DESCRICAO_PARECER'
      'FROM TIPO_PARECER P'
      'WHERE P.CODIGO_TIPO_PARECER =:CODIGO')
    Left = 633
    Top = 48
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsTipoParecer: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspTipoParecer'
    Left = 633
    Top = 184
    object cdsTipoParecerCODIGO_TIPO_PARECER: TIntegerField
      Tag = 7
      DisplayLabel = 'Codigo'
      FieldName = 'CODIGO_TIPO_PARECER'
      Required = True
    end
    object cdsTipoParecerDESCRICAO_PARECER: TStringField
      Tag = 7
      DisplayLabel = 'Descricao'
      FieldName = 'DESCRICAO_PARECER'
      Required = True
      Size = 65
    end
  end
  object dspParecer: TDataSetProvider
    DataSet = fsqParecer
    Left = 737
    Top = 104
  end
  object fsqParecer: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT'
      'P.CODIGO_PARECER,'
      'P.CODIGO_TIPO_PARECER,'
      'P.CODIGO_FUNCIONARIO,'
      'P.OBSERVACAO_PARECER,'
      'P.DATA_PARECER'
      ''
      ''
      'FROM'
      ''
      'PARECER  P'
      'WHERE P.CODIGO_PARECER =:CODIGO')
    Left = 737
    Top = 48
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsParecer: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspParecer'
    Left = 737
    Top = 184
    object cdsParecerCODIGO_PARECER: TIntegerField
      Tag = 7
      DisplayLabel = 'C'#243'digo'
      FieldName = 'CODIGO_PARECER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsParecerCODIGO_TIPO_PARECER: TIntegerField
      FieldName = 'CODIGO_TIPO_PARECER'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsParecerCODIGO_FUNCIONARIO: TIntegerField
      Tag = 8
      FieldName = 'CODIGO_FUNCIONARIO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsParecerDATA_PARECER: TDateField
      Tag = 7
      DisplayLabel = 'Data'
      FieldName = 'DATA_PARECER'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsParecerOBSERVACAO_PARECER: TMemoField
      Tag = 8
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO_PARECER'
      BlobType = ftMemo
    end
  end
  object dspTipos: TDataSetProvider
    DataSet = fsqTipos
    Left = 841
    Top = 103
  end
  object fsqTipos: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT P.CODIGO_TIPO_PARECER, P.DESCRICAO_PARECER'
      'FROM TIPO_PARECER P')
    Left = 841
    Top = 47
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsTipos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspTipos'
    Left = 841
    Top = 183
    object cdsTiposCODIGO_TIPO_PARECER: TIntegerField
      FieldName = 'CODIGO_TIPO_PARECER'
      Required = True
    end
    object cdsTiposDESCRICAO_PARECER: TStringField
      FieldName = 'DESCRICAO_PARECER'
      Required = True
      Size = 65
    end
  end
  object dspLogin: TDataSetProvider
    DataSet = fsqLogin
    Left = 936
    Top = 102
  end
  object fsqLogin: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT '
      'CODIGO_FUNCIONARIO, LOGIN_FUNCIONARIO'
      'FROM FUNCIONARIO F'
      '')
    Left = 936
    Top = 46
    ParamData = <
      item
        Position = 1
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object cdsLogin: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspLogin'
    Left = 936
    Top = 182
    object cdsLoginLOGIN_FUNCIONARIO: TStringField
      FieldName = 'LOGIN_FUNCIONARIO'
      Size = 65
    end
    object cdsLoginCODIGO_FUNCIONARIO: TIntegerField
      FieldName = 'CODIGO_FUNCIONARIO'
      Required = True
    end
  end
end
