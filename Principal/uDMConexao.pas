unit uDMConexao;

interface

uses
  System.SysUtils, System.Classes, Data.DBXInterBase, Data.DB, Data.SqlExpr,
  Data.DBXFirebird, IniFiles, Forms, JvJCLUtils, Data.FMTBcd, Datasnap.Provider,
  Datasnap.DBClient, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.IBBase, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;


type
  TConfigura = record
   CaminhoBanco  :String;
   FBClient      :String;
   Usuario       :String;
   Senha         :String;
   Versao        :String;
  end;

type
  TDMConexao = class(TDataModule)
    sqcConexaoPrincipal: TSQLConnection;
    dspUsuario: TDataSetProvider;
    cdsFuncao: TClientDataSet;
    dspFuncao: TDataSetProvider;
    cdsFuncaoCODIGO_FUNCAO: TIntegerField;
    cdsFuncaoDESCRICAO_FUNCAO: TStringField;
    FDConnection: TFDConnection;
    FDPhysFBDriverLink: TFDPhysFBDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    fsqUsuario: TFDQuery;
    cdsUsuario: TClientDataSet;
    cdsUsuarioUSUA_CODIGO: TIntegerField;
    cdsUsuarioUSUA_NOME: TStringField;
    cdsUsuarioUSUA_LOGIN: TStringField;
    cdsUsuarioUSUA_SENHA: TStringField;
    fsqFucao: TFDQuery;
    dspFuncionario: TDataSetProvider;
    fsqFuncionario: TFDQuery;
    cdsFuncionario: TClientDataSet;
    cdsFuncionarioCODIGO_FUNCIONARIO: TIntegerField;
    cdsFuncionarioNOME_FUNCIONARIO: TStringField;
    cdsFuncionarioSALARIO_FUNCIONARIO: TFMTBCDField;
    cdsFuncionarioDTAADM_FUNCIONARIO: TDateField;
    cdsFuncionarioDTADEM_FUNCIONARIO: TDateField;
    cdsFuncionarioOBS_FUNCIONARIO: TMemoField;
    cdsFuncionarioCODIGO_FUNCAO: TIntegerField;
    cdsFuncionarioFOTO_FUNCIONARIO: TBlobField;
    cdsFuncionarioLOGIN_FUNCIONARIO: TStringField;
    fsqFuncoes: TFDQuery;
    cdsFuncoes: TClientDataSet;
    dspFuncoes: TDataSetProvider;
    cdsFuncoesCODIGO_FUNCAO: TIntegerField;
    cdsFuncoesDESCRICAO_FUNCAO: TStringField;
    dspTipoParecer: TDataSetProvider;
    fsqTipoParecer: TFDQuery;
    cdsTipoParecer: TClientDataSet;
    cdsTipoParecerDESCRICAO_PARECER: TStringField;
    dspParecer: TDataSetProvider;
    fsqParecer: TFDQuery;
    cdsParecer: TClientDataSet;
    cdsParecerCODIGO_PARECER: TIntegerField;
    cdsParecerCODIGO_TIPO_PARECER: TIntegerField;
    cdsParecerCODIGO_FUNCIONARIO: TIntegerField;
    cdsParecerDATA_PARECER: TDateField;
    cdsParecerOBSERVACAO_PARECER: TMemoField;
    dspTipos: TDataSetProvider;
    fsqTipos: TFDQuery;
    cdsTipos: TClientDataSet;
    cdsTipoParecerCODIGO_TIPO_PARECER: TIntegerField;
    cdsTiposCODIGO_TIPO_PARECER: TIntegerField;
    cdsTiposDESCRICAO_PARECER: TStringField;
    dspLogin: TDataSetProvider;
    fsqLogin: TFDQuery;
    cdsLogin: TClientDataSet;
    cdsLoginLOGIN_FUNCIONARIO: TStringField;
    cdsLoginCODIGO_FUNCIONARIO: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    Configuracao: TIniFile;
    procedure ConectionDatabase;
  public
    { Public declarations }
    FConfig: TConfigura;
  end;

var
  DMConexao: TDMConexao;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TDMConexao.ConectionDatabase;
begin
  sqcConexaoPrincipal.Close;
  sqcConexaoPrincipal.ConnectionName   := 'BANCO';
  sqcConexaoPrincipal.DriverName       := 'Interbase';
  sqcConexaoPrincipal.GetDriverFunc    := 'getSQLDriverINTERBASE';
  sqcConexaoPrincipal.LibraryName      := 'dbxint30.dll';

  sqcConexaoPrincipal.Params.Clear;
  sqcConexaoPrincipal.Params.add('drivername=FIREBIRD');
  sqcConexaoPrincipal.Params.add('librarynameosx=libsqlfb.dylib');
  sqcConexaoPrincipal.Params.add('vendorlibwin64=fbclient.dll');
  sqcConexaoPrincipal.Params.add('vendorlibosx=/Library/Frameworks/Firebird.framework/Firebird');
  sqcConexaoPrincipal.Params.add('Database=' + FConfig.CaminhoBanco);
  sqcConexaoPrincipal.Params.add('password=' + FConfig.Senha);
  sqcConexaoPrincipal.Params.add('sqldialect=3');
  sqcConexaoPrincipal.Params.add('user_name=' + FConfig.Usuario);
  sqcConexaoPrincipal.Params.add('trim char=False');
  sqcConexaoPrincipal.Open;

  FDConnection.Params.Values['Server']        := '127.0.0.1/3051';
  FDConnection.Params.Values['Database']      := FConfig.CaminhoBanco;
  FDConnection.Params.Values['User_Name']     := FConfig.Usuario;
  FDConnection.Params.Values['Password']      := FConfig.Senha;
  FDConnection.Params.Values['CharacterSet']  := 'WIN1252';

  try
   FDConnection.Open();
  except
   on E:Exception do
//    raise ShowMessage('Ocorreu o seguinte erro ao conectar no banco de dados: %s', [E.Message]);
  end;
end;

procedure TDMConexao.DataModuleCreate(Sender: TObject);
begin
  Configuracao          := TIniFile.Create(Copy(ExtractFilePath(Application.ExeName), 1,Length(ExtractFilePath(Application.ExeName))-4) + 'Database\Configuracao.ini');
  FConfig.CaminhoBanco  := UpperCase(Configuracao.ReadString('ACESSO', 'caminho', ''));
  FConfig.Usuario       := UpperCase(Configuracao.ReadString('ACESSO', 'usuario', ''));
  FConfig.Senha         := (Configuracao.ReadString('ACESSO', 'senha', ''));
  FConfig.FBClient      := UpperCase(Configuracao.ReadString('ACESSO', 'fbclient', ''));
  FConfig.Versao        := UpperCase(Configuracao.ReadString('ACESSO', 'versao', ''));

  ConectionDatabase;


end;

end.
