unit uDashboard;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinLilian,
  dxSkinsDefaultPainters, cxCustomData, Data.DB, cxDBData, Data.FMTBcd,
  Datasnap.Provider, Datasnap.DBClient, Data.SqlExpr, cxGridCustomView,
  cxGridChartView, cxGridDBChartView, cxClasses, cxGridLevel, cxGrid;

type
  TfrmDashboard = class(TForm)
    DataSource1: TDataSource;
    sqqFuncionario: TSQLQuery;
    cdsFuncionario: TClientDataSet;
    dspFuncionario: TDataSetProvider;
    sqqFuncionarioDESCRICAO_FUNCAO: TStringField;
    cdsFuncionarioDESCRICAO_FUNCAO: TStringField;
    sqqFuncionarioTOTAL: TIntegerField;
    cdsFuncionarioTOTAL: TIntegerField;
    cxGrid2: TcxGrid;
    cxGridLevel1: TcxGridLevel;
    cxGrid2ChartViewPie: TcxGridChartView;
    cxGrid1: TcxGrid;
    cxGridChartViewColumn: TcxGridChartView;
    cxGridLevel2: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }

    procedure GerarGraficoPizza(GridChartView: TcxGridChartView);
    procedure GerarGraficoBarra(GridChartView: TcxGridChartView);
  public
    { Public declarations }
  end;

var
  frmDashboard: TfrmDashboard;

implementation

{$R *.dfm}

uses uDMConexao;

procedure TfrmDashboard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmDashboard := Nil;
end;

procedure TfrmDashboard.FormCreate(Sender: TObject);
var ASeries: TcxGridChartSeries;
    i: Integer;
begin
   GerarGraficoPizza(cxGrid2ChartViewPie);
   GerarGraficoBarra(cxGridChartViewColumn);
end;

procedure TfrmDashboard.GerarGraficoBarra(GridChartView: TcxGridChartView);
var i: Integer;
    ASeries: TcxGridChartSeries;
begin
  cdsFuncionario.Close;
  cdsFuncionario.Open;

  cdsFuncionario.First;

  GridChartView.Title.Text:= 'Dasboard Funcion�rio - Gr�fico de Barras' ;
  GridChartView.DiagramPie.Active := False;

   GridChartView.ClearSeries;
   i:=0;
   while not cdsFuncionario.Eof do
    begin
     with GridChartView.CreateSeries do
      begin
       DisplayText:= cdsFuncionarioDESCRICAO_FUNCAO.AsString;
       AddValue (cdsFuncionarioTOTAL.AsFloat);
       GridChartView.Categories.Values [i]:= cdsFuncionarioDESCRICAO_FUNCAO.AsString;
       i:= i + 1;
      end;
      cdsFuncionario.next;
    end;
   GridChartView.DiagramColumn.Active := True
end;

procedure TfrmDashboard.GerarGraficoPizza(GridChartView: TcxGridChartView);
var i: Integer;
    ASeries: TcxGridChartSeries;
begin
  cdsFuncionario.Close;
  cdsFuncionario.Open;

  cdsFuncionario.First;

  GridChartView.Title.Text:= 'Dasboard Funcion�rio - Gr�fico Pizza' ;
  GridChartView.DiagramPie.Active := False;

   GridChartView.ClearSeries;
   with GridChartView.CreateSeries do
    begin
      i:=0;
      DisplayText:= 'Gr�fico';
      while not cdsFuncionario.Eof do
       begin
         AddValue (cdsFuncionarioTOTAL.AsFloat);
         GridChartView.Categories.Values [i]:= cdsFuncionarioDESCRICAO_FUNCAO.AsString;
         i:= i + 1;
         cdsFuncionario.next;
        end;
    end;

   GridChartView.DiagramPie.Active := True
end;

end.
