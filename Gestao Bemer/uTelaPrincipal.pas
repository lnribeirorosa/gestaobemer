unit uTelaPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uMestre, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, JvGIF, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, JvComponentBase, JvCipher, Vcl.Imaging.pngimage;

type
  TfrmTelaPrincipal = class(TfrmMestre)
    MainMenu1: TMainMenu;
    Ajuda1: TMenuItem;
    Cadastros1: TMenuItem;
    Usurios1: TMenuItem;
    Relatrios1: TMenuItem;
    Consultas1: TMenuItem;
    Label1: TLabel;
    Funo1: TMenuItem;
    Funcionario1: TMenuItem;
    Relatorio1: TMenuItem;
    ipoParecer1: TMenuItem;
    procedure Ajuda1Click(Sender: TObject);
    procedure Usurios1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Funo1Click(Sender: TObject);
    procedure Consultas1Click(Sender: TObject);
    procedure Funcionario1Click(Sender: TObject);
    procedure ipoParecer1Click(Sender: TObject);
    procedure Relatorio1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTelaPrincipal: TfrmTelaPrincipal;

implementation

{$R *.dfm}

uses uCadUsuario, uDMConexao, uFuncoesComuns, uFuncaoFuncionario,
  uDashboard, uFuncionario, uTipoParecer, uRelatorioParecer;

procedure TfrmTelaPrincipal.Ajuda1Click(Sender: TObject);
begin
  inherited;
  ShowMessage(DMConexao.FConfig.Versao);
end;


procedure TfrmTelaPrincipal.Consultas1Click(Sender: TObject);
begin
  inherited;
  frmDashboard := TfrmDashboard.Create(nil);
  frmDashboard.Show;

end;

procedure TfrmTelaPrincipal.FormCreate(Sender: TObject);
begin
  inherited;
  if DMConexao.sqcConexaoPrincipal.Connected then
  begin
    Label1.Caption := 'Banco conectado: ' + DMConexao.FConfig.CaminhoBanco;
  end;
end;

procedure TfrmTelaPrincipal.FormShow(Sender: TObject);
begin
  inherited;
  Self.Top    := 0;
  Self.Left   := 0;
  Self.Height := Screen.WorkAreaHeight;
  Self.Width  := Screen.WorkAreaWidth
end;

procedure TfrmTelaPrincipal.Funcionario1Click(Sender: TObject);
begin
  inherited;
  frmCadFuncinario := TfrmCadFuncinario.Create(nil);
  frmCadFuncinario.ShowModal;

end;

procedure TfrmTelaPrincipal.Funo1Click(Sender: TObject);
begin
  inherited;
  frmCadFuncao := TfrmCadFuncao.Create(nil);
  frmCadFuncao.ShowModal;

end;

procedure TfrmTelaPrincipal.ipoParecer1Click(Sender: TObject);
begin
  inherited;
  frmCadTipoParecer := TfrmCadTipoParecer.Create(nil);
  frmCadTipoParecer.ShowModal;

end;

procedure TfrmTelaPrincipal.Relatorio1Click(Sender: TObject);
begin
  inherited;
  frmRelatorio := TfrmRelatorio.Create(nil);
  frmRelatorio.ShowModal;
end;

procedure TfrmTelaPrincipal.Usurios1Click(Sender: TObject);
begin
  inherited;
  frmCadUsuario := TfrmCadUsuario.Create(nil);
  frmCadUsuario.ShowModal;

end;

end.
