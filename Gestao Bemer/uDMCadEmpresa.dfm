object DMCadEmpresa: TDMCadEmpresa
  OldCreateOrder = False
  Height = 258
  Width = 427
  object sqqEMPRESA: TSQLQuery
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    SQL.Strings = (
      'SELECT EMP_CODIGO,'
      'EMP_RAZAO,'
      'EMP_FANTASIA,'
      'EMP_CNPJ,'
      'EMP_IE,'
      'EMP_TELEFONE,'
      'EMP_TELEFONE1,'
      'EMP_TELEFONE2,'
      'EMP_LOGRADOURO,'
      'CIDA_CODIGO,'
      'EMP_CEP,'
      'EMP_EMAIL'
      'FROM EMPRESA'
      'WHERE  EMP_CODIGO =:CODIGO')
    SQLConnection = DMConexao.sqcConexaoPrincipal
    Left = 24
    Top = 16
    object sqqEMPRESAEMP_CODIGO: TIntegerField
      FieldName = 'EMP_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqqEMPRESAEMP_RAZAO: TStringField
      FieldName = 'EMP_RAZAO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 200
    end
    object sqqEMPRESAEMP_FANTASIA: TStringField
      FieldName = 'EMP_FANTASIA'
      ProviderFlags = [pfInUpdate]
      Size = 200
    end
    object sqqEMPRESAEMP_CNPJ: TStringField
      FieldName = 'EMP_CNPJ'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object sqqEMPRESAEMP_IE: TStringField
      FieldName = 'EMP_IE'
      ProviderFlags = [pfInUpdate]
    end
    object sqqEMPRESAEMP_TELEFONE: TStringField
      FieldName = 'EMP_TELEFONE'
      ProviderFlags = [pfInUpdate]
    end
    object sqqEMPRESAEMP_TELEFONE1: TStringField
      FieldName = 'EMP_TELEFONE1'
      ProviderFlags = [pfInUpdate]
    end
    object sqqEMPRESAEMP_TELEFONE2: TStringField
      FieldName = 'EMP_TELEFONE2'
      ProviderFlags = [pfInUpdate]
    end
    object sqqEMPRESAEMP_LOGRADOURO: TStringField
      FieldName = 'EMP_LOGRADOURO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 200
    end
    object sqqEMPRESACIDA_CODIGO: TIntegerField
      FieldName = 'CIDA_CODIGO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object sqqEMPRESAEMP_CEP: TStringField
      FieldName = 'EMP_CEP'
      ProviderFlags = [pfInUpdate]
    end
    object sqqEMPRESAEMP_EMAIL: TStringField
      FieldName = 'EMP_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
  end
  object dspEMPRESA: TDataSetProvider
    DataSet = sqqEMPRESA
    Left = 24
    Top = 80
  end
  object cdsEMPRESA: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CODIGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspEMPRESA'
    Left = 24
    Top = 144
    object cdsEMPRESAEMP_RAZAO: TStringField
      Tag = 7
      DisplayLabel = 'Raz'#227'o'
      FieldName = 'EMP_RAZAO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 200
    end
    object cdsEMPRESAEMP_FANTASIA: TStringField
      Tag = 7
      DisplayLabel = 'Fantasia'
      FieldName = 'EMP_FANTASIA'
      ProviderFlags = [pfInUpdate]
      Size = 200
    end
    object cdsEMPRESAEMP_CODIGO: TIntegerField
      Tag = 7
      DisplayLabel = 'C'#243'digo'
      FieldName = 'EMP_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsEMPRESAEMP_CNPJ: TStringField
      DisplayLabel = 'CNPJ'
      FieldName = 'EMP_CNPJ'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsEMPRESAEMP_IE: TStringField
      DisplayLabel = 'IE'
      FieldName = 'EMP_IE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsEMPRESAEMP_TELEFONE: TStringField
      FieldName = 'EMP_TELEFONE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsEMPRESAEMP_TELEFONE1: TStringField
      FieldName = 'EMP_TELEFONE1'
      ProviderFlags = [pfInUpdate]
    end
    object cdsEMPRESAEMP_TELEFONE2: TStringField
      FieldName = 'EMP_TELEFONE2'
      ProviderFlags = [pfInUpdate]
    end
    object cdsEMPRESAEMP_LOGRADOURO: TStringField
      FieldName = 'EMP_LOGRADOURO'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 200
    end
    object cdsEMPRESACIDA_CODIGO: TIntegerField
      FieldName = 'CIDA_CODIGO'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsEMPRESAEMP_CEP: TStringField
      FieldName = 'EMP_CEP'
      ProviderFlags = [pfInUpdate]
    end
    object cdsEMPRESAEMP_EMAIL: TStringField
      FieldName = 'EMP_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
  end
end
