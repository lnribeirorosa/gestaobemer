inherited frmCadFuncinario: TfrmCadFuncinario
  Caption = 'Cadastro de Usu'#225'rios'
  ClientHeight = 471
  ClientWidth = 887
  ExplicitWidth = 903
  ExplicitHeight = 510
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlNavegacao: TPanel
    Width = 887
    ExplicitWidth = 813
  end
  inherited PageControlCadastro: TPageControl
    Width = 887
    Height = 412
    ActivePage = TabInfcadastrais
    ExplicitWidth = 813
    ExplicitHeight = 256
    inherited TabSheetConsulta: TTabSheet
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 805
      ExplicitHeight = 228
      inherited Label3: TLabel
        Top = 371
        Width = 31
        ExplicitTop = 371
      end
      inherited pnlPesquisa: TPanel
        Width = 879
        ExplicitWidth = 805
      end
      inherited cxGrid: TcxGrid
        Width = 873
        Height = 244
        ExplicitWidth = 799
        ExplicitHeight = 88
        inherited cxGridDBTableView1: TcxGridDBTableView
          inherited cxGridDBTableViewEdit: TcxGridDBColumn
            IsCaptionAssigned = True
          end
          object cxGridDBTableViewParecer: TcxGridDBColumn
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
          end
        end
      end
    end
    inherited TabInfcadastrais: TTabSheet
      ExplicitWidth = 805
      ExplicitHeight = 228
      object lblUSUA_NOME: TLabel
        Left = 10
        Top = 5
        Width = 25
        Height = 13
        Caption = 'Login'
        FocusControl = edtFUNC_NOME
      end
      object lblDescricao: TLabel
        Left = 145
        Top = 5
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
        FocusControl = edtDESCRICAO_FUNC
      end
      object lblFuncao: TLabel
        Left = 12
        Top = 53
        Width = 35
        Height = 13
        Caption = 'Fun'#231#227'o'
        FocusControl = lkeDescricaoFuncao
      end
      object lblSalario: TLabel
        Left = 145
        Top = 53
        Width = 32
        Height = 13
        Caption = 'Sal'#225'rio'
        FocusControl = edtSalario
      end
      object lblAdm: TLabel
        Left = 303
        Top = 53
        Width = 71
        Height = 13
        Caption = 'Data Admiss'#227'o'
        FocusControl = ddteAdmissao
      end
      object lblDesligamento: TLabel
        Left = 457
        Top = 53
        Width = 90
        Height = 13
        Caption = 'Data Desligamento'
        FocusControl = ddteDesligamento
      end
      object Label8: TLabel
        Left = 10
        Top = 95
        Width = 58
        Height = 13
        Caption = 'Observa'#231#227'o'
      end
      object SpeedButton1: TSpeedButton
        Left = 457
        Top = 22
        Width = 138
        Height = 22
        Caption = 'Foto'
        OnClick = SpeedButton1Click
      end
      object Image1: TImage
        Left = 601
        Top = 21
        Width = 275
        Height = 360
      end
      object edtFUNC_NOME: TDBEdit
        Tag = 1
        Left = 10
        Top = 21
        Width = 121
        Height = 21
        DataField = 'LOGIN_FUNCIONARIO'
        DataSource = dtsCadastro
        TabOrder = 0
      end
      object edtDESCRICAO_FUNC: TDBEdit
        Tag = 1
        Left = 145
        Top = 21
        Width = 296
        Height = 21
        DataField = 'NOME_FUNCIONARIO'
        DataSource = dtsCadastro
        TabOrder = 1
      end
      object edtSalario: TcxDBCalcEdit
        Tag = 1
        Left = 145
        Top = 69
        DataBinding.DataField = 'SALARIO_FUNCIONARIO'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 3
        Width = 152
      end
      object ddteAdmissao: TcxDBDateEdit
        Tag = 1
        Left = 303
        Top = 69
        DataBinding.DataField = 'DTAADM_FUNCIONARIO'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 4
        Width = 138
      end
      object ddteDesligamento: TcxDBDateEdit
        Left = 457
        Top = 69
        DataBinding.DataField = 'DTADEM_FUNCIONARIO'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 5
        Width = 138
      end
      object dbmObs: TcxDBMemo
        Left = 10
        Top = 111
        DataBinding.DataField = 'OBS_FUNCIONARIO'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 6
        Height = 270
        Width = 585
      end
      object lkeDescricaoFuncao: TcxLookupComboBox
        Tag = 1
        Left = 10
        Top = 70
        TabStop = False
        Properties.KeyFieldNames = 'CODIGO_FUNCAO'
        Properties.ListColumns = <
          item
            FieldName = 'DESCRICAO_FUNCAO'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dtsFuncao
        TabOrder = 2
        OnExit = lkeDescricaoFuncaoExit
        Width = 121
      end
    end
  end
  inherited cdsRegistros: TClientDataSet
    Left = 264
    Top = 234
  end
  inherited dtsRegistros: TDataSource
    Left = 328
    Top = 234
  end
  inherited cdsCadastro: TClientDataSet
    Left = 112
    Top = 232
  end
  inherited dtsCadastro: TDataSource
    OnDataChange = dtsCadastroDataChange
    Left = 184
    Top = 232
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 508
    Top = 211
  end
  object dtsFuncao: TDataSource
    DataSet = DMConexao.cdsFuncoes
    Left = 332
    Top = 187
  end
end
