unit uFuncaoFuncionario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uCadastroMestre, Data.DB,
  Datasnap.DBClient, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Mask, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinLilian,
  dxSkinsDefaultPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxButtonEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid;

type
  TfrmCadFuncao = class(TfrmCadastroMestre)
    Label4: TLabel;
    DBEdit1: TDBEdit;
    lblUSUA_NOME: TLabel;
    edtDESCRICAO_FUNCAO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Filtrar; override;
  end;

var
  frmCadFuncao: TfrmCadFuncao;

implementation

{$R *.dfm}

uses uDMConexao;

procedure TfrmCadFuncao.Filtrar;
begin
  inherited;

end;

procedure TfrmCadFuncao.FormCreate(Sender: TObject);
begin
  inherited;
  cdsCadastro                         := DMConexao.cdsFuncao;
  Cadastro.TabelaPrincipal            := 'FUNCAO';
  Cadastro.ChavePrimaria              := 'CODIGO_FUNCAO';
  Cadastro.ShowRegistrosAberturaTela  := True;

end;

end.
