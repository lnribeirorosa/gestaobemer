program dprGestao;

uses
  Vcl.Forms,
  uMestre in '..\Principal\uMestre.pas' {frmMestre},
  uTelaPrincipal in 'uTelaPrincipal.pas' {frmTelaPrincipal},
  uCadastroMestre in '..\Principal\uCadastroMestre.pas' {frmCadastroMestre},
  uFramePesquisa in '..\Principal\uFramePesquisa.pas' {FramePesquisa: TFrame},
  uCadUsuario in 'uCadUsuario.pas' {frmCadUsuario},
  uDMConexao in '..\Principal\uDMConexao.pas' {DMConexao: TDataModule},
  uFuncoesComuns in '..\Principal\uFuncoesComuns.pas',
  uLogin in '..\Principal\uLogin.pas' {frmLogin},
  uDMCadEmpresa in 'uDMCadEmpresa.pas' {DMCadEmpresa: TDataModule},
  uFuncaoFuncionario in 'uFuncaoFuncionario.pas' {frmCadFuncao},
  uDashboard in '..\Principal\uDashboard.pas' {frmDashboard},
  uFuncionario in 'uFuncionario.pas' {frmCadFuncinario},
  uTipoParecer in 'uTipoParecer.pas' {frmCadTipoParecer},
  uParecer in 'uParecer.pas' {frmCadParecer},
  uRelatorioParecer in 'uRelatorioParecer.pas' {frmRelatorio};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Gest�o';
  Application.CreateForm(TDMConexao, DMConexao);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.Run;
end.

