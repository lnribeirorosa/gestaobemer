unit uFuncionario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uCadastroMestre, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinLilian,
  dxSkinsDefaultPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxButtonEdit, cxContainer, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  System.ImageList, Vcl.ImgList, Datasnap.DBClient, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, cxMemo,
  cxDBEdit, cxCalendar, cxCalc, Vcl.ExtDlgs, cxImage, Vcl.Imaging.pngimage;

type
  TfrmCadFuncinario = class(TfrmCadastroMestre)
    lblUSUA_NOME: TLabel;
    edtFUNC_NOME: TDBEdit;
    edtDESCRICAO_FUNC: TDBEdit;
    lblDescricao: TLabel;
    lblFuncao: TLabel;
    edtSalario: TcxDBCalcEdit;
    lblSalario: TLabel;
    ddteAdmissao: TcxDBDateEdit;
    lblAdm: TLabel;
    lblDesligamento: TLabel;
    ddteDesligamento: TcxDBDateEdit;
    dbmObs: TcxDBMemo;
    Label8: TLabel;
    SpeedButton1: TSpeedButton;
    OpenPictureDialog1: TOpenPictureDialog;
    Image1: TImage;
    lkeDescricaoFuncao: TcxLookupComboBox;
    dtsFuncao: TDataSource;
    cxGridDBTableViewParecer: TcxGridDBColumn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LoadFotoFuncionario;
    procedure dtsCadastroDataChange(Sender: TObject; Field: TField);
    procedure lkeDescricaoFuncaoExit(Sender: TObject);
    procedure cxGridDBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Filtrar; override;

  end;

var
  frmCadFuncinario: TfrmCadFuncinario;

implementation

{$R *.dfm}

uses uDMConexao, uParecer;

procedure TfrmCadFuncinario.cxGridDBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  AColumn: TcxGridDBColumn;
begin
  inherited;

  AColumn := ACellViewInfo.Item as TcxGridDBColumn;

  if AColumn.Name = 'cxGridDBTableViewParecer' then
   begin
    frmCadParecer                    := TfrmCadParecer.Create(nil);
    frmCadParecer.CodigoFuncionario  := cxGridDBTableView1.DataController.DataSet.FieldByName('CODIGO_FUNCIONARIO').AsInteger;
    frmCadParecer.ShowModal;
   end;
end;

procedure TfrmCadFuncinario.dtsCadastroDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  LoadFotoFuncionario;
  lkeDescricaoFuncao.EditValue := 0;
  if cdsCadastro.FieldByName('CODIGO_FUNCAO').AsInteger > 0 then
   begin
    lkeDescricaoFuncao.EditValue := cdsCadastro.FieldByName('CODIGO_FUNCAO').AsInteger
   end;
end;

procedure TfrmCadFuncinario.Filtrar;
begin
  inherited;

end;

procedure TfrmCadFuncinario.FormCreate(Sender: TObject);
begin
  inherited;
  cdsCadastro                        := DMConexao.cdsFuncionario;
  Cadastro.TabelaPrincipal           := 'FUNCIONARIO';
  Cadastro.ChavePrimaria             := 'CODIGO_FUNCIONARIO';
  Cadastro.ShowRegistrosAberturaTela := True;


  DMConexao.cdsFuncoes.Close;
  DMConexao.cdsFuncoes.Open;

end;

procedure TfrmCadFuncinario.lkeDescricaoFuncaoExit(Sender: TObject);
begin
  inherited;
  cdsCadastro.FieldByName('CODIGO_FUNCAO').Value := lkeDescricaoFuncao.EditValue;
end;

procedure TfrmCadFuncinario.LoadFotoFuncionario;
var  LStream : TMemoryStream;
begin
  LStream:= TMemoryStream.Create;
  try

   (cdsCadastro.FieldByName('FOTO_FUNCIONARIO') as TBlobField).SaveToStream(LStream);
   LStream.Position:= 0;

   Image1.Picture.LoadFromStream(LStream);

  finally
   LStream.Free;
  end;

end;

procedure TfrmCadFuncinario.SpeedButton1Click(Sender: TObject);
var  LStream : TMemoryStream;
     Png: TPicture;
begin
  inherited;
  if OpenPictureDialog1.Execute then
  begin
    if OpenPictureDialog1.FileName <>'' then
    begin

     Image1.Picture.LoadFromFile(OpenPictureDialog1.FileName);
     LStream:= TMemoryStream.Create;
     Png:= TPicture.Create;
     try
      Png.Assign(Image1.Picture);
      Png.SaveToStream(LStream);
      LStream.Position:= 0;
      (cdsCadastro.FieldByName('FOTO_FUNCIONARIO') as TBlobField).LoadFromStream(LStream);
     finally
      LStream.Free;
      Png.Free;
     end;
  end;
  end;
end;

end.
