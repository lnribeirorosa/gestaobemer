inherited frmCadFuncao: TfrmCadFuncao
  Caption = 'Cadastro de Fun'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControlCadastro: TPageControl
    ActivePage = TabInfcadastrais
    inherited TabSheetConsulta: TTabSheet
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inherited Label3: TLabel
        Width = 31
      end
      inherited cxGrid: TcxGrid
        inherited cxGridDBTableView1: TcxGridDBTableView
          inherited cxGridDBTableViewEdit: TcxGridDBColumn
            IsCaptionAssigned = True
          end
        end
      end
    end
    inherited TabInfcadastrais: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 915
      ExplicitHeight = 350
      object Label4: TLabel
        Left = 3
        Top = 2
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
        FocusControl = DBEdit1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblUSUA_NOME: TLabel
        Left = 3
        Top = 45
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
        FocusControl = edtDESCRICAO_FUNCAO
      end
      object DBEdit1: TDBEdit
        Tag = 1
        Left = 3
        Top = 18
        Width = 190
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'CODIGO_FUNCAO'
        DataSource = dtsCadastro
        ReadOnly = True
        TabOrder = 1
      end
      object edtDESCRICAO_FUNCAO: TDBEdit
        Tag = 1
        Left = 3
        Top = 61
        Width = 190
        Height = 21
        DataField = 'DESCRICAO_FUNCAO'
        DataSource = dtsCadastro
        TabOrder = 0
      end
    end
  end
end
