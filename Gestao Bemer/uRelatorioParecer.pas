unit uRelatorioParecer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinLilian, dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Vcl.StdCtrls, Vcl.ComCtrls,
  dxCore, cxDateUtils, cxCalendar, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxButtonEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, ppDB,
  ppDBPipe, ppParameter, ppDesignLayer, ppBands, ppClass, ppPrnabl, ppCtrls,
  ppCache, ppComm, ppRelatv, ppProd, ppReport, Datasnap.DBClient,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider, System.DateUtils,
  ppStrtch, ppMemo;

type
  TfrmRelatorio = class(TForm)
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    lblFuncao: TLabel;
    lkeDescricaoFuncao: TcxLookupComboBox;
    Label1: TLabel;
    lkeLogin: TcxLookupComboBox;
    dtDataIni: TcxDateEdit;
    Label2: TLabel;
    edtDataFim: TcxDateEdit;
    Label3: TLabel;
    cxGrid: TcxGrid;
    cxGridDBTableViewRelatorio: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dstRelatorio: TDataSource;
    dspRelatorio: TDataSetProvider;
    fsqRelatorio: TFDQuery;
    cdsRelatorio: TClientDataSet;
    ppRelatorio: TppReport;
    ppDesignLayers16: TppDesignLayers;
    ppDesignLayer16: TppDesignLayer;
    ppParameterList3: TppParameterList;
    pplRelatorio: TppDBPipeline;
    dtsFuncoes: TDataSource;
    dtsLogin: TDataSource;
    cxGridDBTableViewRelatorioLOGIN_FUNCIONARIO: TcxGridDBColumn;
    cxGridDBTableViewRelatorioDESCRICAO_PARECER: TcxGridDBColumn;
    cxGridDBTableViewRelatorioOBSERVACAO_PARECER: TcxGridDBColumn;
    cxGridDBTableViewRelatorioDATA_PARECER: TcxGridDBColumn;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppDBMemo1: TppDBMemo;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelatorio: TfrmRelatorio;

implementation

{$R *.dfm}

uses uDMConexao, uFuncoesComuns;

procedure TfrmRelatorio.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmRelatorio := nil;
end;

procedure TfrmRelatorio.FormCreate(Sender: TObject);
begin
  dtDataIni.Text  :=  DateToStr(StartOfTheMonth(date));
  edtDataFim.Text :=  DateToStr(date);

  DMConexao.cdsLogin.Close;
  DMConexao.cdsLogin.Open;

  DMConexao.cdsLogin.Append;
  DMConexao.cdsLoginCODIGO_FUNCIONARIO.AsInteger := 0;
  DMConexao.cdsLoginLOGIN_FUNCIONARIO.AsString   := 'Todos';
  DMConexao.cdsLogin.Post;

  DMConexao.cdsFuncoes.Close;
  DMConexao.cdsFuncoes.Open;

  DMConexao.cdsFuncoes.Append;
  DMConexao.cdsFuncoesCODIGO_FUNCAO.AsInteger := 0;
  DMConexao.cdsFuncoesDESCRICAO_FUNCAO.AsString := 'Todos';
  DMConexao.cdsFuncoes.Post;

end;

procedure TfrmRelatorio.SpeedButton1Click(Sender: TObject);
var Sql: TStringList;
begin
    Sql := TStringList.Create;
    try
     if ((dtDataIni.Text = '') or (edtDataFim.Text = ''))  then
      begin
       TFuncoes.MensagemPadrao('As Datas devem ser informadas!', 0);
       Exit;
      end;

      if ((StrToDate(dtDataIni.Text)) >= (StrToDate(edtDataFim.Text))) then
       begin
        TFuncoes.MensagemPadrao('A Data Inicial deve ser menor que a Data Final!', 0);
        Exit;
       end;

     Sql.Add(' SELECT F.LOGIN_FUNCIONARIO, F.CODIGO_FUNCIONARIO,  F.DTAADM_FUNCIONARIO, F.NOME_FUNCIONARIO,     ');
     Sql.Add('        T.DESCRICAO_PARECER,                                                                      ');
     Sql.Add('        P.OBSERVACAO_PARECER, P.DATA_PARECER                                                      ');
     Sql.Add(' FROM PARECER P                                                                                   ');
     Sql.Add(' LEFT JOIN TIPO_PARECER T ON (T.CODIGO_TIPO_PARECER = P.CODIGO_TIPO_PARECER)                      ');
     Sql.Add(' LEFT JOIN FUNCIONARIO F ON (F.CODIGO_FUNCIONARIO = P.CODIGO_FUNCIONARIO)                         ');
     Sql.Add(' WHERE P.DATA_PARECER BETWEEN :DataIni and :DataFim');


     if lkeDescricaoFuncao.EditValue > 0 then
      begin
        Sql.Add('  AND F.CODIGO_FUNCAO = ' + IntToStr(lkeDescricaoFuncao.EditValue));
      end;

     if lkeLogin.EditValue > 0 then
      begin

       Sql.Add(' AND P.CODIGO_FUNCIONARIO = ' +  IntToStr(lkeLogin.EditValue));
      end;

     Sql.Add(' ORDER BY F.NOME_FUNCIONARIO, P.CODIGO_FUNCIONARIO');
     fsqRelatorio.SQL                                     := Sql;
     dstRelatorio.DataSet                                 := nil;
     cdsRelatorio.Close;

     cxGridDBTableViewRelatorio.DataController.DataSource := nil;
     cdsRelatorio.FetchParams;

     cdsRelatorio.ParamByName('DataIni').AsDate           := dtDataIni.Date;
     cdsRelatorio.ParamByName('DataFim').AsDate           := edtDataFim.Date;

     cdsRelatorio.Open;

     dstRelatorio.DataSet                                 :=  cdsRelatorio;
     cxGridDBTableViewRelatorio.DataController.DataSource :=  dstRelatorio;
    finally
     Sql.Free;
    end;

end;

procedure TfrmRelatorio.SpeedButton2Click(Sender: TObject);
begin
  if cdsRelatorio.RecordCount = 0  then
   begin
    TFuncoes.MensagemPadrao('N�o h� informa��es a serem impressas!', 0);
    Exit;
   end;

 cdsRelatorio.DisableControls;
 try
   ppRelatorio.Print;
 finally
   cdsRelatorio.EnableControls;
 end;
end;

end.
