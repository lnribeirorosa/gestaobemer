unit uDMCadEmpresa;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Datasnap.DBClient,
  Datasnap.Provider, Data.DB, Data.SqlExpr;

type
  TDMCadEmpresa = class(TDataModule)
    sqqEMPRESA: TSQLQuery;
    dspEMPRESA: TDataSetProvider;
    cdsEMPRESA: TClientDataSet;
    sqqEMPRESAEMP_CODIGO: TIntegerField;
    sqqEMPRESAEMP_RAZAO: TStringField;
    sqqEMPRESAEMP_FANTASIA: TStringField;
    sqqEMPRESAEMP_CNPJ: TStringField;
    sqqEMPRESAEMP_IE: TStringField;
    sqqEMPRESAEMP_TELEFONE: TStringField;
    sqqEMPRESAEMP_TELEFONE1: TStringField;
    sqqEMPRESAEMP_TELEFONE2: TStringField;
    sqqEMPRESAEMP_LOGRADOURO: TStringField;
    sqqEMPRESACIDA_CODIGO: TIntegerField;
    sqqEMPRESAEMP_CEP: TStringField;
    sqqEMPRESAEMP_EMAIL: TStringField;
    cdsEMPRESAEMP_RAZAO: TStringField;
    cdsEMPRESAEMP_FANTASIA: TStringField;
    cdsEMPRESAEMP_CODIGO: TIntegerField;
    cdsEMPRESAEMP_CNPJ: TStringField;
    cdsEMPRESAEMP_IE: TStringField;
    cdsEMPRESAEMP_TELEFONE: TStringField;
    cdsEMPRESAEMP_TELEFONE1: TStringField;
    cdsEMPRESAEMP_TELEFONE2: TStringField;
    cdsEMPRESAEMP_LOGRADOURO: TStringField;
    cdsEMPRESACIDA_CODIGO: TIntegerField;
    cdsEMPRESAEMP_CEP: TStringField;
    cdsEMPRESAEMP_EMAIL: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMCadEmpresa: TDMCadEmpresa;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

uses uDMConexao, uFuncoesComuns;

{$R *.dfm}

end.
