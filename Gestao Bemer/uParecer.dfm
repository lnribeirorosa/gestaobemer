inherited frmCadParecer: TfrmCadParecer
  Caption = 'frmCadParecer'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlNavegacao: TPanel
    inherited btnGravar: TBitBtn
      Top = 18
      ExplicitTop = 18
    end
  end
  inherited PageControlCadastro: TPageControl
    ActivePage = TabInfcadastrais
    inherited TabSheetConsulta: TTabSheet
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inherited Label3: TLabel
        Width = 31
      end
      inherited cxGrid: TcxGrid
        inherited cxGridDBTableView1: TcxGridDBTableView
          inherited cxGridDBTableViewEdit: TcxGridDBColumn
            IsCaptionAssigned = True
          end
        end
      end
    end
    inherited TabInfcadastrais: TTabSheet
      object Label4: TLabel
        Left = 12
        Top = 9
        Width = 20
        Height = 13
        Caption = 'Tipo'
        FocusControl = lkeDescricaoParecer
      end
      object Label6: TLabel
        Left = 159
        Top = 12
        Width = 23
        Height = 13
        Caption = 'Data'
        FocusControl = ddtData
      end
      object Label8: TLabel
        Left = 12
        Top = 55
        Width = 58
        Height = 13
        Caption = 'Observa'#231#227'o'
      end
      object lkeDescricaoParecer: TcxLookupComboBox
        Tag = 1
        Left = 12
        Top = 28
        TabStop = False
        Properties.KeyFieldNames = 'CODIGO_TIPO_PARECER'
        Properties.ListColumns = <
          item
            FieldName = 'DESCRICAO_PARECER'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dtsTipoParecer
        TabOrder = 0
        Width = 121
      end
      object ddtData: TcxDBDateEdit
        Tag = 1
        Left = 159
        Top = 28
        DataBinding.DataField = 'DATA_PARECER'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 1
        Width = 138
      end
      object dbmObs: TcxDBMemo
        Left = 12
        Top = 71
        DataBinding.DataField = 'OBSERVACAO_PARECER'
        DataBinding.DataSource = dtsCadastro
        TabOrder = 2
        Height = 113
        Width = 585
      end
    end
  end
  inherited dtsRegistros: TDataSource
    OnDataChange = dtsRegistrosDataChange
  end
  object dtsTipoParecer: TDataSource
    DataSet = DMConexao.cdsTipos
    Left = 396
    Top = 331
  end
end
