object frmRelatorio: TfrmRelatorio
  Left = 0
  Top = 0
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Relat'#243'rio'
  ClientHeight = 285
  ClientWidth = 989
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 567
    Top = 30
    Width = 57
    Height = 22
    Caption = 'Filtrar'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 630
    Top = 30
    Width = 57
    Height = 22
    Caption = 'Imprimir'
    OnClick = SpeedButton2Click
  end
  object lblFuncao: TLabel
    Left = 151
    Top = 13
    Width = 35
    Height = 13
    Caption = 'Fun'#231#227'o'
    FocusControl = lkeDescricaoFuncao
  end
  object Label1: TLabel
    Left = 10
    Top = 13
    Width = 25
    Height = 13
    Caption = 'Login'
    FocusControl = lkeLogin
  end
  object Label2: TLabel
    Left = 299
    Top = 13
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    FocusControl = lkeDescricaoFuncao
  end
  object Label3: TLabel
    Left = 435
    Top = 13
    Width = 48
    Height = 13
    Caption = 'Data Final'
    FocusControl = lkeDescricaoFuncao
  end
  object lkeDescricaoFuncao: TcxLookupComboBox
    Tag = 1
    Left = 149
    Top = 30
    TabStop = False
    Properties.KeyFieldNames = 'CODIGO_FUNCAO'
    Properties.ListColumns = <
      item
        FieldName = 'DESCRICAO_FUNCAO'
      end>
    Properties.ListOptions.ShowHeader = False
    Properties.ListSource = dtsFuncoes
    TabOrder = 0
    Width = 121
  end
  object lkeLogin: TcxLookupComboBox
    Tag = 1
    Left = 8
    Top = 30
    TabStop = False
    Properties.KeyFieldNames = 'CODIGO_FUNCIONARIO'
    Properties.ListColumns = <
      item
        FieldName = 'LOGIN_FUNCIONARIO'
      end>
    Properties.ListOptions.ShowHeader = False
    Properties.ListSource = dtsLogin
    TabOrder = 1
    Width = 121
  end
  object dtDataIni: TcxDateEdit
    Left = 299
    Top = 30
    TabOrder = 2
    Width = 121
  end
  object edtDataFim: TcxDateEdit
    Left = 435
    Top = 30
    TabOrder = 3
    Width = 121
  end
  object cxGrid: TcxGrid
    Left = 8
    Top = 71
    Width = 937
    Height = 202
    TabOrder = 4
    object cxGridDBTableViewRelatorio: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Hint = 'Incluir'
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = True
      DataController.DataSource = dstRelatorio
      DataController.Filter.AutoDataSetFilter = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = 'N'#227'o a dados a serem demonstrados.'
      OptionsView.ShowEditButtons = gsebAlways
      OptionsView.GroupByBox = False
      object cxGridDBTableViewRelatorioLOGIN_FUNCIONARIO: TcxGridDBColumn
        Caption = 'Login'
        DataBinding.FieldName = 'LOGIN_FUNCIONARIO'
      end
      object cxGridDBTableViewRelatorioDESCRICAO_PARECER: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'DESCRICAO_PARECER'
        Width = 42
      end
      object cxGridDBTableViewRelatorioOBSERVACAO_PARECER: TcxGridDBColumn
        Caption = 'Descri'#231#227'o'
        DataBinding.FieldName = 'OBSERVACAO_PARECER'
        Options.Filtering = False
        Width = 294
      end
      object cxGridDBTableViewRelatorioDATA_PARECER: TcxGridDBColumn
        Caption = 'Data'
        DataBinding.FieldName = 'DATA_PARECER'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableViewRelatorio
    end
  end
  object dstRelatorio: TDataSource
    DataSet = cdsRelatorio
    Left = 328
    Top = 232
  end
  object dspRelatorio: TDataSetProvider
    DataSet = fsqRelatorio
    Left = 328
    Top = 136
  end
  object fsqRelatorio: TFDQuery
    Connection = DMConexao.FDConnection
    SQL.Strings = (
      
        ' SELECT F.LOGIN_FUNCIONARIO, F.CODIGO_FUNCIONARIO,  F.DTAADM_FUN' +
        'CIONARIO, F.NOME_FUNCIONARIO,'
      '        T.DESCRICAO_PARECER,'
      '        P.OBSERVACAO_PARECER, P.DATA_PARECER'
      ' FROM PARECER P '
      
        ' LEFT JOIN TIPO_PARECER T ON (T.CODIGO_TIPO_PARECER = P.CODIGO_T' +
        'IPO_PARECER) '
      
        ' LEFT JOIN FUNCIONARIO F ON (F.CODIGO_FUNCIONARIO = P.CODIGO_FUN' +
        'CIONARIO) '
      ' WHERE P.DATA_PARECER BETWEEN :DATAINI AND :DATAFIM')
    Left = 328
    Top = 80
    ParamData = <
      item
        Name = 'DATAINI'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATAFIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object cdsRelatorio: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRelatorio'
    Left = 328
    Top = 184
  end
  object ppRelatorio: TppReport
    AutoStop = False
    DataPipeline = pplRelatorio
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Of'#237'cio 216 x 356mm'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 355600
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 5
    Template.FileName = 'C:\LN\Gestao\Relatorio\RelParecer.rtm'
    Units = utMMThousandths
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = False
    OutlineSettings.CreatePageNodes = False
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    ThumbnailSettings.PageHighlight.Width = 3
    PDFSettings.EmbedFontOptions = []
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PDFSettings.PDFAFormat = pafNone
    PreviewFormSettings.PageBorder.mmPadding = 0
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 128
    Top = 88
    Version = '19.03'
    mmColumnWidth = 0
    DataPipelineName = 'pplRelatorio'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 9525
      mmPrintPosition = 0
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer16
        UserName = 'Label6'
        Border.mmPadding = 0
        Caption = 'Gest'#227'o Bemer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5027
        mmLeft = 89959
        mmTop = 2910
        mmWidth = 28575
        BandType = 0
        LayerName = Foreground14
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 17198
      mmPrintPosition = 0
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer16
        UserName = 'Label4'
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Tipo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4763
        mmLeft = 4493
        mmTop = 5560
        mmWidth = 9260
        BandType = 4
        LayerName = Foreground14
      end
      object ppDBText4: TppDBText
        DesignLayer = ppDesignLayer16
        UserName = 'DBText4'
        Border.mmPadding = 0
        DataField = 'DESCRICAO_PARECER'
        DataPipeline = pplRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'pplRelatorio'
        mmHeight = 4763
        mmLeft = 16128
        mmTop = 5560
        mmWidth = 198375
        BandType = 4
        LayerName = Foreground14
      end
      object ppDBMemo1: TppDBMemo
        DesignLayer = ppDesignLayer16
        UserName = 'DBMemo1'
        Border.mmPadding = 0
        CharWrap = True
        DataField = 'OBSERVACAO_PARECER'
        DataPipeline = pplRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        RemoveEmptyLines = False
        Transparent = True
        DataPipelineName = 'pplRelatorio'
        mmHeight = 6350
        mmLeft = 4498
        mmTop = 10843
        mmWidth = 210005
        BandType = 4
        LayerName = Foreground14
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeading = 0
      end
      object ppLabel5: TppLabel
        DesignLayer = ppDesignLayer16
        UserName = 'Label5'
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Data:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4763
        mmLeft = 4496
        mmTop = 526
        mmWidth = 11377
        BandType = 4
        LayerName = Foreground14
      end
      object ppDBText5: TppDBText
        DesignLayer = ppDesignLayer16
        UserName = 'DBText5'
        Border.mmPadding = 0
        DataField = 'DATA_PARECER'
        DataPipeline = pplRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'pplRelatorio'
        mmHeight = 4763
        mmLeft = 16128
        mmTop = 528
        mmWidth = 32808
        BandType = 4
        LayerName = Foreground14
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object ppGroup1: TppGroup
      BreakName = 'CODIGO_FUNCIONARIO'
      DataPipeline = pplRelatorio
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      KeepTogether = True
      OutlineSettings.CreateNode = True
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'pplRelatorio'
      NewFile = False
      object ppGroupHeaderBand1: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        Border.mmPadding = 0
        mmBottomOffset = 0
        mmHeight = 17727
        mmPrintPosition = 0
        object ppLabel1: TppLabel
          DesignLayer = ppDesignLayer16
          UserName = 'Label1'
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'Login: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial Narrow'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4763
          mmLeft = 4498
          mmTop = 5814
          mmWidth = 9252
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppDBText1: TppDBText
          DesignLayer = ppDesignLayer16
          UserName = 'DBText1'
          Border.mmPadding = 0
          DataField = 'LOGIN_FUNCIONARIO'
          DataPipeline = pplRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 10
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'pplRelatorio'
          mmHeight = 4763
          mmLeft = 14748
          mmTop = 5814
          mmWidth = 105569
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppLabel2: TppLabel
          DesignLayer = ppDesignLayer16
          UserName = 'Label2'
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'Admiss'#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial Narrow'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4763
          mmLeft = 4498
          mmTop = 10841
          mmWidth = 15336
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppDBText2: TppDBText
          DesignLayer = ppDesignLayer16
          UserName = 'DBText2'
          Border.mmPadding = 0
          DataField = 'DTAADM_FUNCIONARIO'
          DataPipeline = pplRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 10
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'pplRelatorio'
          mmHeight = 4763
          mmLeft = 20578
          mmTop = 10841
          mmWidth = 32808
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppLabel3: TppLabel
          DesignLayer = ppDesignLayer16
          UserName = 'Label3'
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'C'#243'digo:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial Narrow'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4763
          mmLeft = 4498
          mmTop = 258
          mmWidth = 14552
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppDBText3: TppDBText
          DesignLayer = ppDesignLayer16
          UserName = 'DBText3'
          Border.mmPadding = 0
          DataField = 'CODIGO_FUNCIONARIO'
          DataPipeline = pplRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 10
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'pplRelatorio'
          mmHeight = 4763
          mmLeft = 19783
          mmTop = 258
          mmWidth = 32808
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
        object ppLine2: TppLine
          DesignLayer = ppDesignLayer16
          UserName = 'Line2'
          Border.mmPadding = 0
          Weight = 0.750000000000000000
          mmHeight = 1320
          mmLeft = -1588
          mmTop = 16398
          mmWidth = 352955
          BandType = 3
          GroupNo = 0
          LayerName = Foreground14
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Background.Brush.Style = bsClear
        Border.mmPadding = 0
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 2117
        mmPrintPosition = 0
        object ppLine1: TppLine
          DesignLayer = ppDesignLayer16
          UserName = 'Line1'
          Border.mmPadding = 0
          Weight = 0.750000000000000000
          mmHeight = 1055
          mmLeft = 1588
          mmTop = 1058
          mmWidth = 352955
          BandType = 5
          GroupNo = 0
          LayerName = Foreground14
        end
      end
    end
    object ppDesignLayers16: TppDesignLayers
      object ppDesignLayer16: TppDesignLayer
        UserName = 'Foreground14'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList3: TppParameterList
    end
  end
  object pplRelatorio: TppDBPipeline
    DataSource = dstRelatorio
    UserName = 'lRelatorio'
    Left = 129
    Top = 146
  end
  object dtsFuncoes: TDataSource
    DataSet = DMConexao.cdsFuncoes
    Left = 208
    Top = 104
  end
  object dtsLogin: TDataSource
    DataSet = DMConexao.cdsLogin
    Left = 208
    Top = 160
  end
end
