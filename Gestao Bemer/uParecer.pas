unit uParecer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uCadastroMestre, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinLilian,
  dxSkinsDefaultPainters, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxButtonEdit, cxContainer, cxMemo, cxDBEdit, cxDropDownEdit, cxCalendar,
  cxTextEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  Datasnap.DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.ComCtrls,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfrmCadParecer = class(TfrmCadastroMestre)
    Label4: TLabel;
    lkeDescricaoParecer: TcxLookupComboBox;
    Label6: TLabel;
    ddtData: TcxDBDateEdit;
    dbmObs: TcxDBMemo;
    Label8: TLabel;
    dtsTipoParecer: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dtsRegistrosDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FCodigoFuncionario: Integer;
  public
    { Public declarations }
    property CodigoFuncionario: Integer read FCodigoFuncionario write FCodigoFuncionario;
    procedure Filtrar; override;
  end;

var
  frmCadParecer: TfrmCadParecer;

implementation

{$R *.dfm}

uses uDMConexao;

procedure TfrmCadParecer.btnGravarClick(Sender: TObject);
begin
  cdsCadastro.FieldByName('CODIGO_FUNCIONARIO').AsInteger :=  CodigoFuncionario;
  cdsCadastro.FieldByName('CODIGO_TIPO_PARECER').Value    := lkeDescricaoParecer.EditValue;

  inherited;

end;

procedure TfrmCadParecer.dtsRegistrosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  lkeDescricaoParecer.EditValue := 0;
  if cdsCadastro.FieldByName('CODIGO_TIPO_PARECER').AsInteger > 0 then
   begin
    lkeDescricaoParecer.EditValue := cdsCadastro.FieldByName('CODIGO_TIPO_PARECER').AsInteger
   end;
end;

procedure TfrmCadParecer.Filtrar;
begin
  inherited;
  cdsRegistros.Filter := 'CODIGO_FUNCIONARIO = ' + IntToStr(CodigoFuncionario);
  cdsRegistros.Filtered := True;

  cdsRegistros.FieldByName('CODIGO_FUNCIONARIO').Visible := False;
end;

procedure TfrmCadParecer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //inherited;
  frmCadParecer := nil;
end;

procedure TfrmCadParecer.FormCreate(Sender: TObject);
begin
  inherited;

  cdsCadastro                        := DMConexao.cdsParecer;
  Cadastro.TabelaPrincipal           := 'PARECER';
  Cadastro.ChavePrimaria             := 'CODIGO_PARECER';
  Cadastro.ShowRegistrosAberturaTela := True;

  DMConexao.cdsTipos.Close;
  DMConexao.cdsTipos.Open;

end;

end.
